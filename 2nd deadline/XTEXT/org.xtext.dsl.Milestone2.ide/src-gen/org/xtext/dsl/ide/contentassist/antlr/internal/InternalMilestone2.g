/*
 * generated by Xtext 2.15.0
 */
grammar InternalMilestone2;

options {
	superClass=AbstractInternalContentAssistParser;
}

@lexer::header {
package org.xtext.dsl.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.xtext.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.dsl.services.Milestone2GrammarAccess;

}
@parser::members {
	private Milestone2GrammarAccess grammarAccess;

	public void setGrammarAccess(Milestone2GrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}

	@Override
	protected Grammar getGrammar() {
		return grammarAccess.getGrammar();
	}

	@Override
	protected String getValueForTokenName(String tokenName) {
		return tokenName;
	}
}

// Entry rule entryRuleEvaluation
entryRuleEvaluation
:
{ before(grammarAccess.getEvaluationRule()); }
	 ruleEvaluation
{ after(grammarAccess.getEvaluationRule()); } 
	 EOF 
;

// Rule Evaluation
ruleEvaluation 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getEvaluationAccess().getBinaryOperationParserRuleCall()); }
		ruleBinaryOperation
		{ after(grammarAccess.getEvaluationAccess().getBinaryOperationParserRuleCall()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleBinaryOperation
entryRuleBinaryOperation
:
{ before(grammarAccess.getBinaryOperationRule()); }
	 ruleBinaryOperation
{ after(grammarAccess.getBinaryOperationRule()); } 
	 EOF 
;

// Rule BinaryOperation
ruleBinaryOperation 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getBinaryOperationAccess().getGroup()); }
		(rule__BinaryOperation__Group__0)
		{ after(grammarAccess.getBinaryOperationAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleUnaryOperation
entryRuleUnaryOperation
:
{ before(grammarAccess.getUnaryOperationRule()); }
	 ruleUnaryOperation
{ after(grammarAccess.getUnaryOperationRule()); } 
	 EOF 
;

// Rule UnaryOperation
ruleUnaryOperation 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getUnaryOperationAccess().getAlternatives()); }
		(rule__UnaryOperation__Alternatives)
		{ after(grammarAccess.getUnaryOperationAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleLitteral
entryRuleLitteral
:
{ before(grammarAccess.getLitteralRule()); }
	 ruleLitteral
{ after(grammarAccess.getLitteralRule()); } 
	 EOF 
;

// Rule Litteral
ruleLitteral 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getLitteralAccess().getAlternatives()); }
		(rule__Litteral__Alternatives)
		{ after(grammarAccess.getLitteralAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleBinaryOperator
entryRuleBinaryOperator
:
{ before(grammarAccess.getBinaryOperatorRule()); }
	 ruleBinaryOperator
{ after(grammarAccess.getBinaryOperatorRule()); } 
	 EOF 
;

// Rule BinaryOperator
ruleBinaryOperator 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getAlternatives()); }
		(rule__BinaryOperator__Alternatives)
		{ after(grammarAccess.getBinaryOperatorAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleVariable
entryRuleVariable
:
{ before(grammarAccess.getVariableRule()); }
	 ruleVariable
{ after(grammarAccess.getVariableRule()); } 
	 EOF 
;

// Rule Variable
ruleVariable 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getVariableAccess().getIdAssignment()); }
		(rule__Variable__IdAssignment)
		{ after(grammarAccess.getVariableAccess().getIdAssignment()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleConstant
entryRuleConstant
:
{ before(grammarAccess.getConstantRule()); }
	 ruleConstant
{ after(grammarAccess.getConstantRule()); } 
	 EOF 
;

// Rule Constant
ruleConstant 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getConstantAccess().getValAssignment()); }
		(rule__Constant__ValAssignment)
		{ after(grammarAccess.getConstantAccess().getValAssignment()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getUnaryOperationAccess().getGroup_0()); }
		(rule__UnaryOperation__Group_0__0)
		{ after(grammarAccess.getUnaryOperationAccess().getGroup_0()); }
	)
	|
	(
		{ before(grammarAccess.getUnaryOperationAccess().getGroup_1()); }
		(rule__UnaryOperation__Group_1__0)
		{ after(grammarAccess.getUnaryOperationAccess().getGroup_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Litteral__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getLitteralAccess().getConstantParserRuleCall_0()); }
		ruleConstant
		{ after(grammarAccess.getLitteralAccess().getConstantParserRuleCall_0()); }
	)
	|
	(
		{ before(grammarAccess.getLitteralAccess().getVariableParserRuleCall_1()); }
		ruleVariable
		{ after(grammarAccess.getLitteralAccess().getVariableParserRuleCall_1()); }
	)
	|
	(
		{ before(grammarAccess.getLitteralAccess().getEvaluationParserRuleCall_2()); }
		ruleEvaluation
		{ after(grammarAccess.getLitteralAccess().getEvaluationParserRuleCall_2()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperator__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getCircumflexAccentKeyword_0()); }
		'^'
		{ after(grammarAccess.getBinaryOperatorAccess().getCircumflexAccentKeyword_0()); }
	)
	|
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getVKeyword_1()); }
		'v'
		{ after(grammarAccess.getBinaryOperatorAccess().getVKeyword_1()); }
	)
	|
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getHyphenMinusGreaterThanSignKeyword_2()); }
		'->'
		{ after(grammarAccess.getBinaryOperatorAccess().getHyphenMinusGreaterThanSignKeyword_2()); }
	)
	|
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); }
		'<->'
		{ after(grammarAccess.getBinaryOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); }
	)
	|
	(
		{ before(grammarAccess.getBinaryOperatorAccess().getVerticalLineKeyword_4()); }
		'|'
		{ after(grammarAccess.getBinaryOperatorAccess().getVerticalLineKeyword_4()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Constant__ValAlternatives_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getConstantAccess().getValTrueKeyword_0_0()); }
		'true'
		{ after(grammarAccess.getConstantAccess().getValTrueKeyword_0_0()); }
	)
	|
	(
		{ before(grammarAccess.getConstantAccess().getValFalseKeyword_0_1()); }
		'false'
		{ after(grammarAccess.getConstantAccess().getValFalseKeyword_0_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__BinaryOperation__Group__0__Impl
	rule__BinaryOperation__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBinaryOperationAccess().getUnaryOperationParserRuleCall_0()); }
	ruleUnaryOperation
	{ after(grammarAccess.getBinaryOperationAccess().getUnaryOperationParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__BinaryOperation__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBinaryOperationAccess().getGroup_1()); }
	(rule__BinaryOperation__Group_1__0)?
	{ after(grammarAccess.getBinaryOperationAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__BinaryOperation__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__BinaryOperation__Group_1__0__Impl
	rule__BinaryOperation__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0()); }
	()
	{ after(grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__BinaryOperation__Group_1__1__Impl
	rule__BinaryOperation__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBinaryOperationAccess().getBinaryOperatorParserRuleCall_1_1()); }
	ruleBinaryOperator
	{ after(grammarAccess.getBinaryOperationAccess().getBinaryOperatorParserRuleCall_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__BinaryOperation__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__BinaryOperation__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBinaryOperationAccess().getRightAssignment_1_2()); }
	(rule__BinaryOperation__RightAssignment_1_2)
	{ after(grammarAccess.getBinaryOperationAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__UnaryOperation__Group_0__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_0__0__Impl
	rule__UnaryOperation__Group_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_0__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getLeftParenthesisKeyword_0_0()); }
	'('
	{ after(grammarAccess.getUnaryOperationAccess().getLeftParenthesisKeyword_0_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_0__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_0__1__Impl
	rule__UnaryOperation__Group_0__2
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_0__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getFAssignment_0_1()); }
	(rule__UnaryOperation__FAssignment_0_1)
	{ after(grammarAccess.getUnaryOperationAccess().getFAssignment_0_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_0__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_0__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_0__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_0_2()); }
	')'
	{ after(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_0_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__UnaryOperation__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_1__0__Impl
	rule__UnaryOperation__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getLeftParenthesisHyphenMinusKeyword_1_0()); }
	'(-'
	{ after(grammarAccess.getUnaryOperationAccess().getLeftParenthesisHyphenMinusKeyword_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_1__1__Impl
	rule__UnaryOperation__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getFAssignment_1_1()); }
	(rule__UnaryOperation__FAssignment_1_1)
	{ after(grammarAccess.getUnaryOperationAccess().getFAssignment_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__UnaryOperation__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_1_2()); }
	')'
	{ after(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__BinaryOperation__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0()); }
		ruleUnaryOperation
		{ after(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__FAssignment_0_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_0_1_0()); }
		ruleLitteral
		{ after(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_0_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__UnaryOperation__FAssignment_1_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_1_1_0()); }
		ruleLitteral
		{ after(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_1_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Variable__IdAssignment
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); }
		RULE_ID
		{ after(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Constant__ValAssignment
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getConstantAccess().getValAlternatives_0()); }
		(rule__Constant__ValAlternatives_0)
		{ after(grammarAccess.getConstantAccess().getValAlternatives_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;

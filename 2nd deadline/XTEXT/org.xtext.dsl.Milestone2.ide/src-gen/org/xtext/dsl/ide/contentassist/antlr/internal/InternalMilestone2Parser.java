package org.xtext.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.dsl.services.Milestone2GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMilestone2Parser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'^'", "'v'", "'->'", "'<->'", "'|'", "'true'", "'false'", "'('", "')'", "'(-'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalMilestone2Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMilestone2Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMilestone2Parser.tokenNames; }
    public String getGrammarFileName() { return "InternalMilestone2.g"; }


    	private Milestone2GrammarAccess grammarAccess;

    	public void setGrammarAccess(Milestone2GrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleEvaluation"
    // InternalMilestone2.g:53:1: entryRuleEvaluation : ruleEvaluation EOF ;
    public final void entryRuleEvaluation() throws RecognitionException {
        try {
            // InternalMilestone2.g:54:1: ( ruleEvaluation EOF )
            // InternalMilestone2.g:55:1: ruleEvaluation EOF
            {
             before(grammarAccess.getEvaluationRule()); 
            pushFollow(FOLLOW_1);
            ruleEvaluation();

            state._fsp--;

             after(grammarAccess.getEvaluationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEvaluation"


    // $ANTLR start "ruleEvaluation"
    // InternalMilestone2.g:62:1: ruleEvaluation : ( ruleBinaryOperation ) ;
    public final void ruleEvaluation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:66:2: ( ( ruleBinaryOperation ) )
            // InternalMilestone2.g:67:2: ( ruleBinaryOperation )
            {
            // InternalMilestone2.g:67:2: ( ruleBinaryOperation )
            // InternalMilestone2.g:68:3: ruleBinaryOperation
            {
             before(grammarAccess.getEvaluationAccess().getBinaryOperationParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleBinaryOperation();

            state._fsp--;

             after(grammarAccess.getEvaluationAccess().getBinaryOperationParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEvaluation"


    // $ANTLR start "entryRuleBinaryOperation"
    // InternalMilestone2.g:78:1: entryRuleBinaryOperation : ruleBinaryOperation EOF ;
    public final void entryRuleBinaryOperation() throws RecognitionException {
        try {
            // InternalMilestone2.g:79:1: ( ruleBinaryOperation EOF )
            // InternalMilestone2.g:80:1: ruleBinaryOperation EOF
            {
             before(grammarAccess.getBinaryOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleBinaryOperation();

            state._fsp--;

             after(grammarAccess.getBinaryOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryOperation"


    // $ANTLR start "ruleBinaryOperation"
    // InternalMilestone2.g:87:1: ruleBinaryOperation : ( ( rule__BinaryOperation__Group__0 ) ) ;
    public final void ruleBinaryOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:91:2: ( ( ( rule__BinaryOperation__Group__0 ) ) )
            // InternalMilestone2.g:92:2: ( ( rule__BinaryOperation__Group__0 ) )
            {
            // InternalMilestone2.g:92:2: ( ( rule__BinaryOperation__Group__0 ) )
            // InternalMilestone2.g:93:3: ( rule__BinaryOperation__Group__0 )
            {
             before(grammarAccess.getBinaryOperationAccess().getGroup()); 
            // InternalMilestone2.g:94:3: ( rule__BinaryOperation__Group__0 )
            // InternalMilestone2.g:94:4: rule__BinaryOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryOperation"


    // $ANTLR start "entryRuleUnaryOperation"
    // InternalMilestone2.g:103:1: entryRuleUnaryOperation : ruleUnaryOperation EOF ;
    public final void entryRuleUnaryOperation() throws RecognitionException {
        try {
            // InternalMilestone2.g:104:1: ( ruleUnaryOperation EOF )
            // InternalMilestone2.g:105:1: ruleUnaryOperation EOF
            {
             before(grammarAccess.getUnaryOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleUnaryOperation();

            state._fsp--;

             after(grammarAccess.getUnaryOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnaryOperation"


    // $ANTLR start "ruleUnaryOperation"
    // InternalMilestone2.g:112:1: ruleUnaryOperation : ( ( rule__UnaryOperation__Alternatives ) ) ;
    public final void ruleUnaryOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:116:2: ( ( ( rule__UnaryOperation__Alternatives ) ) )
            // InternalMilestone2.g:117:2: ( ( rule__UnaryOperation__Alternatives ) )
            {
            // InternalMilestone2.g:117:2: ( ( rule__UnaryOperation__Alternatives ) )
            // InternalMilestone2.g:118:3: ( rule__UnaryOperation__Alternatives )
            {
             before(grammarAccess.getUnaryOperationAccess().getAlternatives()); 
            // InternalMilestone2.g:119:3: ( rule__UnaryOperation__Alternatives )
            // InternalMilestone2.g:119:4: rule__UnaryOperation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnaryOperationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryOperation"


    // $ANTLR start "entryRuleLitteral"
    // InternalMilestone2.g:128:1: entryRuleLitteral : ruleLitteral EOF ;
    public final void entryRuleLitteral() throws RecognitionException {
        try {
            // InternalMilestone2.g:129:1: ( ruleLitteral EOF )
            // InternalMilestone2.g:130:1: ruleLitteral EOF
            {
             before(grammarAccess.getLitteralRule()); 
            pushFollow(FOLLOW_1);
            ruleLitteral();

            state._fsp--;

             after(grammarAccess.getLitteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLitteral"


    // $ANTLR start "ruleLitteral"
    // InternalMilestone2.g:137:1: ruleLitteral : ( ( rule__Litteral__Alternatives ) ) ;
    public final void ruleLitteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:141:2: ( ( ( rule__Litteral__Alternatives ) ) )
            // InternalMilestone2.g:142:2: ( ( rule__Litteral__Alternatives ) )
            {
            // InternalMilestone2.g:142:2: ( ( rule__Litteral__Alternatives ) )
            // InternalMilestone2.g:143:3: ( rule__Litteral__Alternatives )
            {
             before(grammarAccess.getLitteralAccess().getAlternatives()); 
            // InternalMilestone2.g:144:3: ( rule__Litteral__Alternatives )
            // InternalMilestone2.g:144:4: rule__Litteral__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Litteral__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLitteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLitteral"


    // $ANTLR start "entryRuleBinaryOperator"
    // InternalMilestone2.g:153:1: entryRuleBinaryOperator : ruleBinaryOperator EOF ;
    public final void entryRuleBinaryOperator() throws RecognitionException {
        try {
            // InternalMilestone2.g:154:1: ( ruleBinaryOperator EOF )
            // InternalMilestone2.g:155:1: ruleBinaryOperator EOF
            {
             before(grammarAccess.getBinaryOperatorRule()); 
            pushFollow(FOLLOW_1);
            ruleBinaryOperator();

            state._fsp--;

             after(grammarAccess.getBinaryOperatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryOperator"


    // $ANTLR start "ruleBinaryOperator"
    // InternalMilestone2.g:162:1: ruleBinaryOperator : ( ( rule__BinaryOperator__Alternatives ) ) ;
    public final void ruleBinaryOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:166:2: ( ( ( rule__BinaryOperator__Alternatives ) ) )
            // InternalMilestone2.g:167:2: ( ( rule__BinaryOperator__Alternatives ) )
            {
            // InternalMilestone2.g:167:2: ( ( rule__BinaryOperator__Alternatives ) )
            // InternalMilestone2.g:168:3: ( rule__BinaryOperator__Alternatives )
            {
             before(grammarAccess.getBinaryOperatorAccess().getAlternatives()); 
            // InternalMilestone2.g:169:3: ( rule__BinaryOperator__Alternatives )
            // InternalMilestone2.g:169:4: rule__BinaryOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BinaryOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBinaryOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryOperator"


    // $ANTLR start "entryRuleVariable"
    // InternalMilestone2.g:178:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalMilestone2.g:179:1: ( ruleVariable EOF )
            // InternalMilestone2.g:180:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalMilestone2.g:187:1: ruleVariable : ( ( rule__Variable__IdAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:191:2: ( ( ( rule__Variable__IdAssignment ) ) )
            // InternalMilestone2.g:192:2: ( ( rule__Variable__IdAssignment ) )
            {
            // InternalMilestone2.g:192:2: ( ( rule__Variable__IdAssignment ) )
            // InternalMilestone2.g:193:3: ( rule__Variable__IdAssignment )
            {
             before(grammarAccess.getVariableAccess().getIdAssignment()); 
            // InternalMilestone2.g:194:3: ( rule__Variable__IdAssignment )
            // InternalMilestone2.g:194:4: rule__Variable__IdAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable__IdAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getIdAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleConstant"
    // InternalMilestone2.g:203:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // InternalMilestone2.g:204:1: ( ruleConstant EOF )
            // InternalMilestone2.g:205:1: ruleConstant EOF
            {
             before(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getConstantRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalMilestone2.g:212:1: ruleConstant : ( ( rule__Constant__ValAssignment ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:216:2: ( ( ( rule__Constant__ValAssignment ) ) )
            // InternalMilestone2.g:217:2: ( ( rule__Constant__ValAssignment ) )
            {
            // InternalMilestone2.g:217:2: ( ( rule__Constant__ValAssignment ) )
            // InternalMilestone2.g:218:3: ( rule__Constant__ValAssignment )
            {
             before(grammarAccess.getConstantAccess().getValAssignment()); 
            // InternalMilestone2.g:219:3: ( rule__Constant__ValAssignment )
            // InternalMilestone2.g:219:4: rule__Constant__ValAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValAssignment();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "rule__UnaryOperation__Alternatives"
    // InternalMilestone2.g:227:1: rule__UnaryOperation__Alternatives : ( ( ( rule__UnaryOperation__Group_0__0 ) ) | ( ( rule__UnaryOperation__Group_1__0 ) ) );
    public final void rule__UnaryOperation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:231:1: ( ( ( rule__UnaryOperation__Group_0__0 ) ) | ( ( rule__UnaryOperation__Group_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==18) ) {
                alt1=1;
            }
            else if ( (LA1_0==20) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMilestone2.g:232:2: ( ( rule__UnaryOperation__Group_0__0 ) )
                    {
                    // InternalMilestone2.g:232:2: ( ( rule__UnaryOperation__Group_0__0 ) )
                    // InternalMilestone2.g:233:3: ( rule__UnaryOperation__Group_0__0 )
                    {
                     before(grammarAccess.getUnaryOperationAccess().getGroup_0()); 
                    // InternalMilestone2.g:234:3: ( rule__UnaryOperation__Group_0__0 )
                    // InternalMilestone2.g:234:4: rule__UnaryOperation__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnaryOperation__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryOperationAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:238:2: ( ( rule__UnaryOperation__Group_1__0 ) )
                    {
                    // InternalMilestone2.g:238:2: ( ( rule__UnaryOperation__Group_1__0 ) )
                    // InternalMilestone2.g:239:3: ( rule__UnaryOperation__Group_1__0 )
                    {
                     before(grammarAccess.getUnaryOperationAccess().getGroup_1()); 
                    // InternalMilestone2.g:240:3: ( rule__UnaryOperation__Group_1__0 )
                    // InternalMilestone2.g:240:4: rule__UnaryOperation__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UnaryOperation__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryOperationAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Alternatives"


    // $ANTLR start "rule__Litteral__Alternatives"
    // InternalMilestone2.g:248:1: rule__Litteral__Alternatives : ( ( ruleConstant ) | ( ruleVariable ) | ( ruleEvaluation ) );
    public final void rule__Litteral__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:252:1: ( ( ruleConstant ) | ( ruleVariable ) | ( ruleEvaluation ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 16:
            case 17:
                {
                alt2=1;
                }
                break;
            case RULE_ID:
                {
                alt2=2;
                }
                break;
            case 18:
            case 20:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMilestone2.g:253:2: ( ruleConstant )
                    {
                    // InternalMilestone2.g:253:2: ( ruleConstant )
                    // InternalMilestone2.g:254:3: ruleConstant
                    {
                     before(grammarAccess.getLitteralAccess().getConstantParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleConstant();

                    state._fsp--;

                     after(grammarAccess.getLitteralAccess().getConstantParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:259:2: ( ruleVariable )
                    {
                    // InternalMilestone2.g:259:2: ( ruleVariable )
                    // InternalMilestone2.g:260:3: ruleVariable
                    {
                     before(grammarAccess.getLitteralAccess().getVariableParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getLitteralAccess().getVariableParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMilestone2.g:265:2: ( ruleEvaluation )
                    {
                    // InternalMilestone2.g:265:2: ( ruleEvaluation )
                    // InternalMilestone2.g:266:3: ruleEvaluation
                    {
                     before(grammarAccess.getLitteralAccess().getEvaluationParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleEvaluation();

                    state._fsp--;

                     after(grammarAccess.getLitteralAccess().getEvaluationParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Litteral__Alternatives"


    // $ANTLR start "rule__BinaryOperator__Alternatives"
    // InternalMilestone2.g:275:1: rule__BinaryOperator__Alternatives : ( ( '^' ) | ( 'v' ) | ( '->' ) | ( '<->' ) | ( '|' ) );
    public final void rule__BinaryOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:279:1: ( ( '^' ) | ( 'v' ) | ( '->' ) | ( '<->' ) | ( '|' ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            case 14:
                {
                alt3=4;
                }
                break;
            case 15:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMilestone2.g:280:2: ( '^' )
                    {
                    // InternalMilestone2.g:280:2: ( '^' )
                    // InternalMilestone2.g:281:3: '^'
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getCircumflexAccentKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getBinaryOperatorAccess().getCircumflexAccentKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:286:2: ( 'v' )
                    {
                    // InternalMilestone2.g:286:2: ( 'v' )
                    // InternalMilestone2.g:287:3: 'v'
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getVKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBinaryOperatorAccess().getVKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMilestone2.g:292:2: ( '->' )
                    {
                    // InternalMilestone2.g:292:2: ( '->' )
                    // InternalMilestone2.g:293:3: '->'
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getBinaryOperatorAccess().getHyphenMinusGreaterThanSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMilestone2.g:298:2: ( '<->' )
                    {
                    // InternalMilestone2.g:298:2: ( '<->' )
                    // InternalMilestone2.g:299:3: '<->'
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getBinaryOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMilestone2.g:304:2: ( '|' )
                    {
                    // InternalMilestone2.g:304:2: ( '|' )
                    // InternalMilestone2.g:305:3: '|'
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getVerticalLineKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getBinaryOperatorAccess().getVerticalLineKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperator__Alternatives"


    // $ANTLR start "rule__Constant__ValAlternatives_0"
    // InternalMilestone2.g:314:1: rule__Constant__ValAlternatives_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__Constant__ValAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:318:1: ( ( 'true' ) | ( 'false' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            else if ( (LA4_0==17) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMilestone2.g:319:2: ( 'true' )
                    {
                    // InternalMilestone2.g:319:2: ( 'true' )
                    // InternalMilestone2.g:320:3: 'true'
                    {
                     before(grammarAccess.getConstantAccess().getValTrueKeyword_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getConstantAccess().getValTrueKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:325:2: ( 'false' )
                    {
                    // InternalMilestone2.g:325:2: ( 'false' )
                    // InternalMilestone2.g:326:3: 'false'
                    {
                     before(grammarAccess.getConstantAccess().getValFalseKeyword_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getConstantAccess().getValFalseKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValAlternatives_0"


    // $ANTLR start "rule__BinaryOperation__Group__0"
    // InternalMilestone2.g:335:1: rule__BinaryOperation__Group__0 : rule__BinaryOperation__Group__0__Impl rule__BinaryOperation__Group__1 ;
    public final void rule__BinaryOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:339:1: ( rule__BinaryOperation__Group__0__Impl rule__BinaryOperation__Group__1 )
            // InternalMilestone2.g:340:2: rule__BinaryOperation__Group__0__Impl rule__BinaryOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__BinaryOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group__0"


    // $ANTLR start "rule__BinaryOperation__Group__0__Impl"
    // InternalMilestone2.g:347:1: rule__BinaryOperation__Group__0__Impl : ( ruleUnaryOperation ) ;
    public final void rule__BinaryOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:351:1: ( ( ruleUnaryOperation ) )
            // InternalMilestone2.g:352:1: ( ruleUnaryOperation )
            {
            // InternalMilestone2.g:352:1: ( ruleUnaryOperation )
            // InternalMilestone2.g:353:2: ruleUnaryOperation
            {
             before(grammarAccess.getBinaryOperationAccess().getUnaryOperationParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryOperation();

            state._fsp--;

             after(grammarAccess.getBinaryOperationAccess().getUnaryOperationParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group__0__Impl"


    // $ANTLR start "rule__BinaryOperation__Group__1"
    // InternalMilestone2.g:362:1: rule__BinaryOperation__Group__1 : rule__BinaryOperation__Group__1__Impl ;
    public final void rule__BinaryOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:366:1: ( rule__BinaryOperation__Group__1__Impl )
            // InternalMilestone2.g:367:2: rule__BinaryOperation__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group__1"


    // $ANTLR start "rule__BinaryOperation__Group__1__Impl"
    // InternalMilestone2.g:373:1: rule__BinaryOperation__Group__1__Impl : ( ( rule__BinaryOperation__Group_1__0 )? ) ;
    public final void rule__BinaryOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:377:1: ( ( ( rule__BinaryOperation__Group_1__0 )? ) )
            // InternalMilestone2.g:378:1: ( ( rule__BinaryOperation__Group_1__0 )? )
            {
            // InternalMilestone2.g:378:1: ( ( rule__BinaryOperation__Group_1__0 )? )
            // InternalMilestone2.g:379:2: ( rule__BinaryOperation__Group_1__0 )?
            {
             before(grammarAccess.getBinaryOperationAccess().getGroup_1()); 
            // InternalMilestone2.g:380:2: ( rule__BinaryOperation__Group_1__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=11 && LA5_0<=15)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalMilestone2.g:380:3: rule__BinaryOperation__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BinaryOperation__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBinaryOperationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group__1__Impl"


    // $ANTLR start "rule__BinaryOperation__Group_1__0"
    // InternalMilestone2.g:389:1: rule__BinaryOperation__Group_1__0 : rule__BinaryOperation__Group_1__0__Impl rule__BinaryOperation__Group_1__1 ;
    public final void rule__BinaryOperation__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:393:1: ( rule__BinaryOperation__Group_1__0__Impl rule__BinaryOperation__Group_1__1 )
            // InternalMilestone2.g:394:2: rule__BinaryOperation__Group_1__0__Impl rule__BinaryOperation__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__BinaryOperation__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__0"


    // $ANTLR start "rule__BinaryOperation__Group_1__0__Impl"
    // InternalMilestone2.g:401:1: rule__BinaryOperation__Group_1__0__Impl : ( () ) ;
    public final void rule__BinaryOperation__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:405:1: ( ( () ) )
            // InternalMilestone2.g:406:1: ( () )
            {
            // InternalMilestone2.g:406:1: ( () )
            // InternalMilestone2.g:407:2: ()
            {
             before(grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0()); 
            // InternalMilestone2.g:408:2: ()
            // InternalMilestone2.g:408:3: 
            {
            }

             after(grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__0__Impl"


    // $ANTLR start "rule__BinaryOperation__Group_1__1"
    // InternalMilestone2.g:416:1: rule__BinaryOperation__Group_1__1 : rule__BinaryOperation__Group_1__1__Impl rule__BinaryOperation__Group_1__2 ;
    public final void rule__BinaryOperation__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:420:1: ( rule__BinaryOperation__Group_1__1__Impl rule__BinaryOperation__Group_1__2 )
            // InternalMilestone2.g:421:2: rule__BinaryOperation__Group_1__1__Impl rule__BinaryOperation__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__BinaryOperation__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__1"


    // $ANTLR start "rule__BinaryOperation__Group_1__1__Impl"
    // InternalMilestone2.g:428:1: rule__BinaryOperation__Group_1__1__Impl : ( ruleBinaryOperator ) ;
    public final void rule__BinaryOperation__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:432:1: ( ( ruleBinaryOperator ) )
            // InternalMilestone2.g:433:1: ( ruleBinaryOperator )
            {
            // InternalMilestone2.g:433:1: ( ruleBinaryOperator )
            // InternalMilestone2.g:434:2: ruleBinaryOperator
            {
             before(grammarAccess.getBinaryOperationAccess().getBinaryOperatorParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleBinaryOperator();

            state._fsp--;

             after(grammarAccess.getBinaryOperationAccess().getBinaryOperatorParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__1__Impl"


    // $ANTLR start "rule__BinaryOperation__Group_1__2"
    // InternalMilestone2.g:443:1: rule__BinaryOperation__Group_1__2 : rule__BinaryOperation__Group_1__2__Impl ;
    public final void rule__BinaryOperation__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:447:1: ( rule__BinaryOperation__Group_1__2__Impl )
            // InternalMilestone2.g:448:2: rule__BinaryOperation__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BinaryOperation__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__2"


    // $ANTLR start "rule__BinaryOperation__Group_1__2__Impl"
    // InternalMilestone2.g:454:1: rule__BinaryOperation__Group_1__2__Impl : ( ( rule__BinaryOperation__RightAssignment_1_2 ) ) ;
    public final void rule__BinaryOperation__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:458:1: ( ( ( rule__BinaryOperation__RightAssignment_1_2 ) ) )
            // InternalMilestone2.g:459:1: ( ( rule__BinaryOperation__RightAssignment_1_2 ) )
            {
            // InternalMilestone2.g:459:1: ( ( rule__BinaryOperation__RightAssignment_1_2 ) )
            // InternalMilestone2.g:460:2: ( rule__BinaryOperation__RightAssignment_1_2 )
            {
             before(grammarAccess.getBinaryOperationAccess().getRightAssignment_1_2()); 
            // InternalMilestone2.g:461:2: ( rule__BinaryOperation__RightAssignment_1_2 )
            // InternalMilestone2.g:461:3: rule__BinaryOperation__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__BinaryOperation__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBinaryOperationAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__Group_1__2__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_0__0"
    // InternalMilestone2.g:470:1: rule__UnaryOperation__Group_0__0 : rule__UnaryOperation__Group_0__0__Impl rule__UnaryOperation__Group_0__1 ;
    public final void rule__UnaryOperation__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:474:1: ( rule__UnaryOperation__Group_0__0__Impl rule__UnaryOperation__Group_0__1 )
            // InternalMilestone2.g:475:2: rule__UnaryOperation__Group_0__0__Impl rule__UnaryOperation__Group_0__1
            {
            pushFollow(FOLLOW_5);
            rule__UnaryOperation__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__0"


    // $ANTLR start "rule__UnaryOperation__Group_0__0__Impl"
    // InternalMilestone2.g:482:1: rule__UnaryOperation__Group_0__0__Impl : ( '(' ) ;
    public final void rule__UnaryOperation__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:486:1: ( ( '(' ) )
            // InternalMilestone2.g:487:1: ( '(' )
            {
            // InternalMilestone2.g:487:1: ( '(' )
            // InternalMilestone2.g:488:2: '('
            {
             before(grammarAccess.getUnaryOperationAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getUnaryOperationAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__0__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_0__1"
    // InternalMilestone2.g:497:1: rule__UnaryOperation__Group_0__1 : rule__UnaryOperation__Group_0__1__Impl rule__UnaryOperation__Group_0__2 ;
    public final void rule__UnaryOperation__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:501:1: ( rule__UnaryOperation__Group_0__1__Impl rule__UnaryOperation__Group_0__2 )
            // InternalMilestone2.g:502:2: rule__UnaryOperation__Group_0__1__Impl rule__UnaryOperation__Group_0__2
            {
            pushFollow(FOLLOW_6);
            rule__UnaryOperation__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__1"


    // $ANTLR start "rule__UnaryOperation__Group_0__1__Impl"
    // InternalMilestone2.g:509:1: rule__UnaryOperation__Group_0__1__Impl : ( ( rule__UnaryOperation__FAssignment_0_1 ) ) ;
    public final void rule__UnaryOperation__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:513:1: ( ( ( rule__UnaryOperation__FAssignment_0_1 ) ) )
            // InternalMilestone2.g:514:1: ( ( rule__UnaryOperation__FAssignment_0_1 ) )
            {
            // InternalMilestone2.g:514:1: ( ( rule__UnaryOperation__FAssignment_0_1 ) )
            // InternalMilestone2.g:515:2: ( rule__UnaryOperation__FAssignment_0_1 )
            {
             before(grammarAccess.getUnaryOperationAccess().getFAssignment_0_1()); 
            // InternalMilestone2.g:516:2: ( rule__UnaryOperation__FAssignment_0_1 )
            // InternalMilestone2.g:516:3: rule__UnaryOperation__FAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperation__FAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getUnaryOperationAccess().getFAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__1__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_0__2"
    // InternalMilestone2.g:524:1: rule__UnaryOperation__Group_0__2 : rule__UnaryOperation__Group_0__2__Impl ;
    public final void rule__UnaryOperation__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:528:1: ( rule__UnaryOperation__Group_0__2__Impl )
            // InternalMilestone2.g:529:2: rule__UnaryOperation__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__2"


    // $ANTLR start "rule__UnaryOperation__Group_0__2__Impl"
    // InternalMilestone2.g:535:1: rule__UnaryOperation__Group_0__2__Impl : ( ')' ) ;
    public final void rule__UnaryOperation__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:539:1: ( ( ')' ) )
            // InternalMilestone2.g:540:1: ( ')' )
            {
            // InternalMilestone2.g:540:1: ( ')' )
            // InternalMilestone2.g:541:2: ')'
            {
             before(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_0_2()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_0__2__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_1__0"
    // InternalMilestone2.g:551:1: rule__UnaryOperation__Group_1__0 : rule__UnaryOperation__Group_1__0__Impl rule__UnaryOperation__Group_1__1 ;
    public final void rule__UnaryOperation__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:555:1: ( rule__UnaryOperation__Group_1__0__Impl rule__UnaryOperation__Group_1__1 )
            // InternalMilestone2.g:556:2: rule__UnaryOperation__Group_1__0__Impl rule__UnaryOperation__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__UnaryOperation__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__0"


    // $ANTLR start "rule__UnaryOperation__Group_1__0__Impl"
    // InternalMilestone2.g:563:1: rule__UnaryOperation__Group_1__0__Impl : ( '(-' ) ;
    public final void rule__UnaryOperation__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:567:1: ( ( '(-' ) )
            // InternalMilestone2.g:568:1: ( '(-' )
            {
            // InternalMilestone2.g:568:1: ( '(-' )
            // InternalMilestone2.g:569:2: '(-'
            {
             before(grammarAccess.getUnaryOperationAccess().getLeftParenthesisHyphenMinusKeyword_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getUnaryOperationAccess().getLeftParenthesisHyphenMinusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__0__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_1__1"
    // InternalMilestone2.g:578:1: rule__UnaryOperation__Group_1__1 : rule__UnaryOperation__Group_1__1__Impl rule__UnaryOperation__Group_1__2 ;
    public final void rule__UnaryOperation__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:582:1: ( rule__UnaryOperation__Group_1__1__Impl rule__UnaryOperation__Group_1__2 )
            // InternalMilestone2.g:583:2: rule__UnaryOperation__Group_1__1__Impl rule__UnaryOperation__Group_1__2
            {
            pushFollow(FOLLOW_6);
            rule__UnaryOperation__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__1"


    // $ANTLR start "rule__UnaryOperation__Group_1__1__Impl"
    // InternalMilestone2.g:590:1: rule__UnaryOperation__Group_1__1__Impl : ( ( rule__UnaryOperation__FAssignment_1_1 ) ) ;
    public final void rule__UnaryOperation__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:594:1: ( ( ( rule__UnaryOperation__FAssignment_1_1 ) ) )
            // InternalMilestone2.g:595:1: ( ( rule__UnaryOperation__FAssignment_1_1 ) )
            {
            // InternalMilestone2.g:595:1: ( ( rule__UnaryOperation__FAssignment_1_1 ) )
            // InternalMilestone2.g:596:2: ( rule__UnaryOperation__FAssignment_1_1 )
            {
             before(grammarAccess.getUnaryOperationAccess().getFAssignment_1_1()); 
            // InternalMilestone2.g:597:2: ( rule__UnaryOperation__FAssignment_1_1 )
            // InternalMilestone2.g:597:3: rule__UnaryOperation__FAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperation__FAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getUnaryOperationAccess().getFAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__1__Impl"


    // $ANTLR start "rule__UnaryOperation__Group_1__2"
    // InternalMilestone2.g:605:1: rule__UnaryOperation__Group_1__2 : rule__UnaryOperation__Group_1__2__Impl ;
    public final void rule__UnaryOperation__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:609:1: ( rule__UnaryOperation__Group_1__2__Impl )
            // InternalMilestone2.g:610:2: rule__UnaryOperation__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperation__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__2"


    // $ANTLR start "rule__UnaryOperation__Group_1__2__Impl"
    // InternalMilestone2.g:616:1: rule__UnaryOperation__Group_1__2__Impl : ( ')' ) ;
    public final void rule__UnaryOperation__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:620:1: ( ( ')' ) )
            // InternalMilestone2.g:621:1: ( ')' )
            {
            // InternalMilestone2.g:621:1: ( ')' )
            // InternalMilestone2.g:622:2: ')'
            {
             before(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_1_2()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__Group_1__2__Impl"


    // $ANTLR start "rule__BinaryOperation__RightAssignment_1_2"
    // InternalMilestone2.g:632:1: rule__BinaryOperation__RightAssignment_1_2 : ( ruleUnaryOperation ) ;
    public final void rule__BinaryOperation__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:636:1: ( ( ruleUnaryOperation ) )
            // InternalMilestone2.g:637:2: ( ruleUnaryOperation )
            {
            // InternalMilestone2.g:637:2: ( ruleUnaryOperation )
            // InternalMilestone2.g:638:3: ruleUnaryOperation
            {
             before(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryOperation();

            state._fsp--;

             after(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperation__RightAssignment_1_2"


    // $ANTLR start "rule__UnaryOperation__FAssignment_0_1"
    // InternalMilestone2.g:647:1: rule__UnaryOperation__FAssignment_0_1 : ( ruleLitteral ) ;
    public final void rule__UnaryOperation__FAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:651:1: ( ( ruleLitteral ) )
            // InternalMilestone2.g:652:2: ( ruleLitteral )
            {
            // InternalMilestone2.g:652:2: ( ruleLitteral )
            // InternalMilestone2.g:653:3: ruleLitteral
            {
             before(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLitteral();

            state._fsp--;

             after(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__FAssignment_0_1"


    // $ANTLR start "rule__UnaryOperation__FAssignment_1_1"
    // InternalMilestone2.g:662:1: rule__UnaryOperation__FAssignment_1_1 : ( ruleLitteral ) ;
    public final void rule__UnaryOperation__FAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:666:1: ( ( ruleLitteral ) )
            // InternalMilestone2.g:667:2: ( ruleLitteral )
            {
            // InternalMilestone2.g:667:2: ( ruleLitteral )
            // InternalMilestone2.g:668:3: ruleLitteral
            {
             before(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLitteral();

            state._fsp--;

             after(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperation__FAssignment_1_1"


    // $ANTLR start "rule__Variable__IdAssignment"
    // InternalMilestone2.g:677:1: rule__Variable__IdAssignment : ( RULE_ID ) ;
    public final void rule__Variable__IdAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:681:1: ( ( RULE_ID ) )
            // InternalMilestone2.g:682:2: ( RULE_ID )
            {
            // InternalMilestone2.g:682:2: ( RULE_ID )
            // InternalMilestone2.g:683:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__IdAssignment"


    // $ANTLR start "rule__Constant__ValAssignment"
    // InternalMilestone2.g:692:1: rule__Constant__ValAssignment : ( ( rule__Constant__ValAlternatives_0 ) ) ;
    public final void rule__Constant__ValAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMilestone2.g:696:1: ( ( ( rule__Constant__ValAlternatives_0 ) ) )
            // InternalMilestone2.g:697:2: ( ( rule__Constant__ValAlternatives_0 ) )
            {
            // InternalMilestone2.g:697:2: ( ( rule__Constant__ValAlternatives_0 ) )
            // InternalMilestone2.g:698:3: ( rule__Constant__ValAlternatives_0 )
            {
             before(grammarAccess.getConstantAccess().getValAlternatives_0()); 
            // InternalMilestone2.g:699:3: ( rule__Constant__ValAlternatives_0 )
            // InternalMilestone2.g:699:4: rule__Constant__ValAlternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Constant__ValAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getValAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000170010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});

}
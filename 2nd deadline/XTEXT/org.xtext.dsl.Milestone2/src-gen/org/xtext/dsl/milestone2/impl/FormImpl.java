/**
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl.milestone2.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.dsl.milestone2.Form;
import org.xtext.dsl.milestone2.Milestone2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FormImpl extends LitteralImpl implements Form
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FormImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Milestone2Package.Literals.FORM;
  }

} //FormImpl

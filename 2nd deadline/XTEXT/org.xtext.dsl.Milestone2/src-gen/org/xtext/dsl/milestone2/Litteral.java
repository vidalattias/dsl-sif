/**
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl.milestone2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Litteral</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.dsl.milestone2.Milestone2Package#getLitteral()
 * @model
 * @generated
 */
public interface Litteral extends EObject
{
} // Litteral

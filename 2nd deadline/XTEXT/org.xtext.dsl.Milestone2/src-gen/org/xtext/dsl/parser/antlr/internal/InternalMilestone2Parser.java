package org.xtext.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.dsl.services.Milestone2GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMilestone2Parser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'('", "')'", "'(-'", "'^'", "'v'", "'->'", "'<->'", "'|'", "'true'", "'false'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalMilestone2Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMilestone2Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMilestone2Parser.tokenNames; }
    public String getGrammarFileName() { return "InternalMilestone2.g"; }



     	private Milestone2GrammarAccess grammarAccess;

        public InternalMilestone2Parser(TokenStream input, Milestone2GrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Evaluation";
       	}

       	@Override
       	protected Milestone2GrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleEvaluation"
    // InternalMilestone2.g:64:1: entryRuleEvaluation returns [EObject current=null] : iv_ruleEvaluation= ruleEvaluation EOF ;
    public final EObject entryRuleEvaluation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvaluation = null;


        try {
            // InternalMilestone2.g:64:51: (iv_ruleEvaluation= ruleEvaluation EOF )
            // InternalMilestone2.g:65:2: iv_ruleEvaluation= ruleEvaluation EOF
            {
             newCompositeNode(grammarAccess.getEvaluationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvaluation=ruleEvaluation();

            state._fsp--;

             current =iv_ruleEvaluation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvaluation"


    // $ANTLR start "ruleEvaluation"
    // InternalMilestone2.g:71:1: ruleEvaluation returns [EObject current=null] : this_BinaryOperation_0= ruleBinaryOperation ;
    public final EObject ruleEvaluation() throws RecognitionException {
        EObject current = null;

        EObject this_BinaryOperation_0 = null;



        	enterRule();

        try {
            // InternalMilestone2.g:77:2: (this_BinaryOperation_0= ruleBinaryOperation )
            // InternalMilestone2.g:78:2: this_BinaryOperation_0= ruleBinaryOperation
            {

            		newCompositeNode(grammarAccess.getEvaluationAccess().getBinaryOperationParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_BinaryOperation_0=ruleBinaryOperation();

            state._fsp--;


            		current = this_BinaryOperation_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvaluation"


    // $ANTLR start "entryRuleBinaryOperation"
    // InternalMilestone2.g:89:1: entryRuleBinaryOperation returns [EObject current=null] : iv_ruleBinaryOperation= ruleBinaryOperation EOF ;
    public final EObject entryRuleBinaryOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryOperation = null;


        try {
            // InternalMilestone2.g:89:56: (iv_ruleBinaryOperation= ruleBinaryOperation EOF )
            // InternalMilestone2.g:90:2: iv_ruleBinaryOperation= ruleBinaryOperation EOF
            {
             newCompositeNode(grammarAccess.getBinaryOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinaryOperation=ruleBinaryOperation();

            state._fsp--;

             current =iv_ruleBinaryOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryOperation"


    // $ANTLR start "ruleBinaryOperation"
    // InternalMilestone2.g:96:1: ruleBinaryOperation returns [EObject current=null] : (this_UnaryOperation_0= ruleUnaryOperation ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )? ) ;
    public final EObject ruleBinaryOperation() throws RecognitionException {
        EObject current = null;

        EObject this_UnaryOperation_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMilestone2.g:102:2: ( (this_UnaryOperation_0= ruleUnaryOperation ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )? ) )
            // InternalMilestone2.g:103:2: (this_UnaryOperation_0= ruleUnaryOperation ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )? )
            {
            // InternalMilestone2.g:103:2: (this_UnaryOperation_0= ruleUnaryOperation ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )? )
            // InternalMilestone2.g:104:3: this_UnaryOperation_0= ruleUnaryOperation ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )?
            {

            			newCompositeNode(grammarAccess.getBinaryOperationAccess().getUnaryOperationParserRuleCall_0());
            		
            pushFollow(FOLLOW_3);
            this_UnaryOperation_0=ruleUnaryOperation();

            state._fsp--;


            			current = this_UnaryOperation_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMilestone2.g:112:3: ( () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=14 && LA1_0<=18)) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalMilestone2.g:113:4: () ruleBinaryOperator ( (lv_right_3_0= ruleUnaryOperation ) )
                    {
                    // InternalMilestone2.g:113:4: ()
                    // InternalMilestone2.g:114:5: 
                    {

                    					current = forceCreateModelElementAndSet(
                    						grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0(),
                    						current);
                    				

                    }


                    				newCompositeNode(grammarAccess.getBinaryOperationAccess().getBinaryOperatorParserRuleCall_1_1());
                    			
                    pushFollow(FOLLOW_4);
                    ruleBinaryOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalMilestone2.g:127:4: ( (lv_right_3_0= ruleUnaryOperation ) )
                    // InternalMilestone2.g:128:5: (lv_right_3_0= ruleUnaryOperation )
                    {
                    // InternalMilestone2.g:128:5: (lv_right_3_0= ruleUnaryOperation )
                    // InternalMilestone2.g:129:6: lv_right_3_0= ruleUnaryOperation
                    {

                    						newCompositeNode(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleUnaryOperation();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBinaryOperationRule());
                    						}
                    						set(
                    							current,
                    							"right",
                    							lv_right_3_0,
                    							"org.xtext.dsl.Milestone2.UnaryOperation");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryOperation"


    // $ANTLR start "entryRuleUnaryOperation"
    // InternalMilestone2.g:151:1: entryRuleUnaryOperation returns [EObject current=null] : iv_ruleUnaryOperation= ruleUnaryOperation EOF ;
    public final EObject entryRuleUnaryOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryOperation = null;


        try {
            // InternalMilestone2.g:151:55: (iv_ruleUnaryOperation= ruleUnaryOperation EOF )
            // InternalMilestone2.g:152:2: iv_ruleUnaryOperation= ruleUnaryOperation EOF
            {
             newCompositeNode(grammarAccess.getUnaryOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnaryOperation=ruleUnaryOperation();

            state._fsp--;

             current =iv_ruleUnaryOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryOperation"


    // $ANTLR start "ruleUnaryOperation"
    // InternalMilestone2.g:158:1: ruleUnaryOperation returns [EObject current=null] : ( (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' ) | (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' ) ) ;
    public final EObject ruleUnaryOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_f_1_0 = null;

        EObject lv_f_4_0 = null;



        	enterRule();

        try {
            // InternalMilestone2.g:164:2: ( ( (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' ) | (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' ) ) )
            // InternalMilestone2.g:165:2: ( (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' ) | (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' ) )
            {
            // InternalMilestone2.g:165:2: ( (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' ) | (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==13) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMilestone2.g:166:3: (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' )
                    {
                    // InternalMilestone2.g:166:3: (otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')' )
                    // InternalMilestone2.g:167:4: otherlv_0= '(' ( (lv_f_1_0= ruleLitteral ) ) otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,11,FOLLOW_5); 

                    				newLeafNode(otherlv_0, grammarAccess.getUnaryOperationAccess().getLeftParenthesisKeyword_0_0());
                    			
                    // InternalMilestone2.g:171:4: ( (lv_f_1_0= ruleLitteral ) )
                    // InternalMilestone2.g:172:5: (lv_f_1_0= ruleLitteral )
                    {
                    // InternalMilestone2.g:172:5: (lv_f_1_0= ruleLitteral )
                    // InternalMilestone2.g:173:6: lv_f_1_0= ruleLitteral
                    {

                    						newCompositeNode(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_f_1_0=ruleLitteral();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUnaryOperationRule());
                    						}
                    						set(
                    							current,
                    							"f",
                    							lv_f_1_0,
                    							"org.xtext.dsl.Milestone2.Litteral");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,12,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:196:3: (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' )
                    {
                    // InternalMilestone2.g:196:3: (otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')' )
                    // InternalMilestone2.g:197:4: otherlv_3= '(-' ( (lv_f_4_0= ruleLitteral ) ) otherlv_5= ')'
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getUnaryOperationAccess().getLeftParenthesisHyphenMinusKeyword_1_0());
                    			
                    // InternalMilestone2.g:201:4: ( (lv_f_4_0= ruleLitteral ) )
                    // InternalMilestone2.g:202:5: (lv_f_4_0= ruleLitteral )
                    {
                    // InternalMilestone2.g:202:5: (lv_f_4_0= ruleLitteral )
                    // InternalMilestone2.g:203:6: lv_f_4_0= ruleLitteral
                    {

                    						newCompositeNode(grammarAccess.getUnaryOperationAccess().getFLitteralParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_f_4_0=ruleLitteral();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUnaryOperationRule());
                    						}
                    						set(
                    							current,
                    							"f",
                    							lv_f_4_0,
                    							"org.xtext.dsl.Milestone2.Litteral");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,12,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getUnaryOperationAccess().getRightParenthesisKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOperation"


    // $ANTLR start "entryRuleLitteral"
    // InternalMilestone2.g:229:1: entryRuleLitteral returns [EObject current=null] : iv_ruleLitteral= ruleLitteral EOF ;
    public final EObject entryRuleLitteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLitteral = null;


        try {
            // InternalMilestone2.g:229:49: (iv_ruleLitteral= ruleLitteral EOF )
            // InternalMilestone2.g:230:2: iv_ruleLitteral= ruleLitteral EOF
            {
             newCompositeNode(grammarAccess.getLitteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLitteral=ruleLitteral();

            state._fsp--;

             current =iv_ruleLitteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLitteral"


    // $ANTLR start "ruleLitteral"
    // InternalMilestone2.g:236:1: ruleLitteral returns [EObject current=null] : (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Evaluation_2= ruleEvaluation ) ;
    public final EObject ruleLitteral() throws RecognitionException {
        EObject current = null;

        EObject this_Constant_0 = null;

        EObject this_Variable_1 = null;

        EObject this_Evaluation_2 = null;



        	enterRule();

        try {
            // InternalMilestone2.g:242:2: ( (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Evaluation_2= ruleEvaluation ) )
            // InternalMilestone2.g:243:2: (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Evaluation_2= ruleEvaluation )
            {
            // InternalMilestone2.g:243:2: (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Evaluation_2= ruleEvaluation )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 19:
            case 20:
                {
                alt3=1;
                }
                break;
            case RULE_ID:
                {
                alt3=2;
                }
                break;
            case 11:
            case 13:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMilestone2.g:244:3: this_Constant_0= ruleConstant
                    {

                    			newCompositeNode(grammarAccess.getLitteralAccess().getConstantParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Constant_0=ruleConstant();

                    state._fsp--;


                    			current = this_Constant_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:253:3: this_Variable_1= ruleVariable
                    {

                    			newCompositeNode(grammarAccess.getLitteralAccess().getVariableParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Variable_1=ruleVariable();

                    state._fsp--;


                    			current = this_Variable_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMilestone2.g:262:3: this_Evaluation_2= ruleEvaluation
                    {

                    			newCompositeNode(grammarAccess.getLitteralAccess().getEvaluationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Evaluation_2=ruleEvaluation();

                    state._fsp--;


                    			current = this_Evaluation_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLitteral"


    // $ANTLR start "entryRuleBinaryOperator"
    // InternalMilestone2.g:274:1: entryRuleBinaryOperator returns [String current=null] : iv_ruleBinaryOperator= ruleBinaryOperator EOF ;
    public final String entryRuleBinaryOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBinaryOperator = null;


        try {
            // InternalMilestone2.g:274:54: (iv_ruleBinaryOperator= ruleBinaryOperator EOF )
            // InternalMilestone2.g:275:2: iv_ruleBinaryOperator= ruleBinaryOperator EOF
            {
             newCompositeNode(grammarAccess.getBinaryOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinaryOperator=ruleBinaryOperator();

            state._fsp--;

             current =iv_ruleBinaryOperator.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryOperator"


    // $ANTLR start "ruleBinaryOperator"
    // InternalMilestone2.g:281:1: ruleBinaryOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '^' | kw= 'v' | kw= '->' | kw= '<->' | kw= '|' ) ;
    public final AntlrDatatypeRuleToken ruleBinaryOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMilestone2.g:287:2: ( (kw= '^' | kw= 'v' | kw= '->' | kw= '<->' | kw= '|' ) )
            // InternalMilestone2.g:288:2: (kw= '^' | kw= 'v' | kw= '->' | kw= '<->' | kw= '|' )
            {
            // InternalMilestone2.g:288:2: (kw= '^' | kw= 'v' | kw= '->' | kw= '<->' | kw= '|' )
            int alt4=5;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt4=1;
                }
                break;
            case 15:
                {
                alt4=2;
                }
                break;
            case 16:
                {
                alt4=3;
                }
                break;
            case 17:
                {
                alt4=4;
                }
                break;
            case 18:
                {
                alt4=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalMilestone2.g:289:3: kw= '^'
                    {
                    kw=(Token)match(input,14,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBinaryOperatorAccess().getCircumflexAccentKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:295:3: kw= 'v'
                    {
                    kw=(Token)match(input,15,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBinaryOperatorAccess().getVKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalMilestone2.g:301:3: kw= '->'
                    {
                    kw=(Token)match(input,16,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBinaryOperatorAccess().getHyphenMinusGreaterThanSignKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalMilestone2.g:307:3: kw= '<->'
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBinaryOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalMilestone2.g:313:3: kw= '|'
                    {
                    kw=(Token)match(input,18,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBinaryOperatorAccess().getVerticalLineKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryOperator"


    // $ANTLR start "entryRuleVariable"
    // InternalMilestone2.g:322:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalMilestone2.g:322:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalMilestone2.g:323:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalMilestone2.g:329:1: ruleVariable returns [EObject current=null] : ( (lv_id_0_0= RULE_ID ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;


        	enterRule();

        try {
            // InternalMilestone2.g:335:2: ( ( (lv_id_0_0= RULE_ID ) ) )
            // InternalMilestone2.g:336:2: ( (lv_id_0_0= RULE_ID ) )
            {
            // InternalMilestone2.g:336:2: ( (lv_id_0_0= RULE_ID ) )
            // InternalMilestone2.g:337:3: (lv_id_0_0= RULE_ID )
            {
            // InternalMilestone2.g:337:3: (lv_id_0_0= RULE_ID )
            // InternalMilestone2.g:338:4: lv_id_0_0= RULE_ID
            {
            lv_id_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_id_0_0, grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableRule());
            				}
            				setWithLastConsumed(
            					current,
            					"id",
            					lv_id_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleConstant"
    // InternalMilestone2.g:357:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // InternalMilestone2.g:357:49: (iv_ruleConstant= ruleConstant EOF )
            // InternalMilestone2.g:358:2: iv_ruleConstant= ruleConstant EOF
            {
             newCompositeNode(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstant=ruleConstant();

            state._fsp--;

             current =iv_ruleConstant; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalMilestone2.g:364:1: ruleConstant returns [EObject current=null] : ( ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) ) ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token lv_val_0_1=null;
        Token lv_val_0_2=null;


        	enterRule();

        try {
            // InternalMilestone2.g:370:2: ( ( ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) ) ) )
            // InternalMilestone2.g:371:2: ( ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) ) )
            {
            // InternalMilestone2.g:371:2: ( ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) ) )
            // InternalMilestone2.g:372:3: ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) )
            {
            // InternalMilestone2.g:372:3: ( (lv_val_0_1= 'true' | lv_val_0_2= 'false' ) )
            // InternalMilestone2.g:373:4: (lv_val_0_1= 'true' | lv_val_0_2= 'false' )
            {
            // InternalMilestone2.g:373:4: (lv_val_0_1= 'true' | lv_val_0_2= 'false' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            else if ( (LA5_0==20) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalMilestone2.g:374:5: lv_val_0_1= 'true'
                    {
                    lv_val_0_1=(Token)match(input,19,FOLLOW_2); 

                    					newLeafNode(lv_val_0_1, grammarAccess.getConstantAccess().getValTrueKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getConstantRule());
                    					}
                    					setWithLastConsumed(current, "val", lv_val_0_1, null);
                    				

                    }
                    break;
                case 2 :
                    // InternalMilestone2.g:385:5: lv_val_0_2= 'false'
                    {
                    lv_val_0_2=(Token)match(input,20,FOLLOW_2); 

                    					newLeafNode(lv_val_0_2, grammarAccess.getConstantAccess().getValFalseKeyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getConstantRule());
                    					}
                    					setWithLastConsumed(current, "val", lv_val_0_2, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000007C002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002800L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000182810L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});

}
/*
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.dsl.milestone2.BinaryOperation;
import org.xtext.dsl.milestone2.Constant;
import org.xtext.dsl.milestone2.Milestone2Package;
import org.xtext.dsl.milestone2.UnaryOperation;
import org.xtext.dsl.milestone2.Variable;
import org.xtext.dsl.services.Milestone2GrammarAccess;

@SuppressWarnings("all")
public class Milestone2SemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private Milestone2GrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == Milestone2Package.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case Milestone2Package.BINARY_OPERATION:
				sequence_BinaryOperation(context, (BinaryOperation) semanticObject); 
				return; 
			case Milestone2Package.CONSTANT:
				sequence_Constant(context, (Constant) semanticObject); 
				return; 
			case Milestone2Package.UNARY_OPERATION:
				sequence_UnaryOperation(context, (UnaryOperation) semanticObject); 
				return; 
			case Milestone2Package.VARIABLE:
				sequence_Variable(context, (Variable) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Evaluation returns BinaryOperation
	 *     BinaryOperation returns BinaryOperation
	 *     Litteral returns BinaryOperation
	 *
	 * Constraint:
	 *     (left=BinaryOperation_BinaryOperation_1_0 right=UnaryOperation)
	 */
	protected void sequence_BinaryOperation(ISerializationContext context, BinaryOperation semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Milestone2Package.Literals.BINARY_OPERATION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Milestone2Package.Literals.BINARY_OPERATION__LEFT));
			if (transientValues.isValueTransient(semanticObject, Milestone2Package.Literals.BINARY_OPERATION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Milestone2Package.Literals.BINARY_OPERATION__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getBinaryOperationAccess().getBinaryOperationLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getBinaryOperationAccess().getRightUnaryOperationParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Litteral returns Constant
	 *     Constant returns Constant
	 *
	 * Constraint:
	 *     (val='true' | val='false')
	 */
	protected void sequence_Constant(ISerializationContext context, Constant semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Evaluation returns UnaryOperation
	 *     BinaryOperation returns UnaryOperation
	 *     BinaryOperation.BinaryOperation_1_0 returns UnaryOperation
	 *     UnaryOperation returns UnaryOperation
	 *     Litteral returns UnaryOperation
	 *
	 * Constraint:
	 *     (f=Litteral | f=Litteral)
	 */
	protected void sequence_UnaryOperation(ISerializationContext context, UnaryOperation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Litteral returns Variable
	 *     Variable returns Variable
	 *
	 * Constraint:
	 *     id=ID
	 */
	protected void sequence_Variable(ISerializationContext context, Variable semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Milestone2Package.Literals.VARIABLE__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Milestone2Package.Literals.VARIABLE__ID));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0(), semanticObject.getId());
		feeder.finish();
	}
	
	
}

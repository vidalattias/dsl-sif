/*
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class Milestone2RuntimeModule extends AbstractMilestone2RuntimeModule {
}

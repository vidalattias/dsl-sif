/**
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl.ui.contentassist;

import org.xtext.dsl.ui.contentassist.AbstractMilestone2ProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class Milestone2ProposalProvider extends AbstractMilestone2ProposalProvider {
}
